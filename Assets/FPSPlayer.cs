﻿using Net.Component;
using Net.Component.Client;
using UnityEngine;
public class FPSPlayer : MonoBehaviour
{
    public string playName;
    internal Net.Vector3 position;
    internal Net.Quaternion rotation;
    internal Net.Vector3 direction;
    public Animator anim;
    public MouseLook mouseX, mouseY;
    public Camera gunCamera;
    public Camera mainCamera;
    public GameObject handle;
    public bool fire, jump;
    public bool isGrounded;
    public Animator gunAnim;
    public Transform fireDir;
    public bool isLoacl;
    public float moveSpeed = 6f;
    public GameObject[] metalImpactPrefabs;
    public float hp = 100;
    public bool isDeath;
    public float jumpForce = 35f;
    private Rigidbody _rigidbody;
    public float height = 1.5f;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        var rigs = anim.GetComponentsInChildren<Rigidbody>();
        var colls = anim.GetComponentsInChildren<Collider>();
        foreach (var rig in rigs)
        {
            rig.isKinematic = true;
        }
        foreach (var coll in colls)
        {
            coll.isTrigger = true;
        }
    }

    private void Update()
    {
        if (isDeath)
            return;
        if (direction != Net.Vector3.zero)
        {
            if (isLoacl)
            {
                transform.Translate(direction * moveSpeed * Time.deltaTime);
                position = transform.position;
                rotation = transform.rotation;
            }
            else 
            {
                transform.position = Vector3.Lerp(transform.position, position, 0.25f);
                transform.rotation = Quaternion.Lerp(transform.rotation, rotation, 0.5f);
            }
            anim.Play("run");
        }
        else 
        {
            if(isGrounded)
                anim.Play("idle");
            if (isLoacl)
            {
                position = transform.position;
                rotation = transform.rotation;
            }
            else 
            {
                transform.position = Vector3.Lerp(transform.position, position, 0.25f);
                transform.rotation = Quaternion.Lerp(transform.rotation, rotation, 0.5f);
            }
        }
        if (fire) 
        {
            fire = false;
            if (isLoacl)
                gunAnim.Play("Fire", 0, 0f);
            else
                anim.Play("shoot");
            Ray ray = new Ray(fireDir.position, fireDir.forward);
            if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity))
            {
                Debug.Log("碰撞对象: " + hit.collider.name);
                Debug.DrawLine(ray.origin, hit.point, Color.red);

                var player = hit.collider.GetComponent<FPSPlayer>();
                if (player != null) 
                    ClientManager.AddOperation(new Net.Share.Operation(Command.Damage, playName, player.playName) { health = 35f });
                var enemy = hit.collider.GetComponent<Enemy>();
                if (enemy != null)
                    ClientManager.AddOperation(new Net.Share.Operation(Command.EnemyDamage, playName, enemy.name) { health = 35f });
                if (enemy != null & isLoacl)
                {
                    enemy.target = this;
                    enemy.localSync = true;
                }
                var bullet = Instantiate(metalImpactPrefabs[Random.Range(0, metalImpactPrefabs.Length)], hit.point, Quaternion.LookRotation(hit.point));
                Destroy(bullet, 5f);
            }
        }
        if (!isGrounded)
        {
            if (Physics.Raycast(transform.position, Vector3.down, out RaycastHit hit, height))
                if (hit.transform != transform)
                    isGrounded = true;
        }
        if (jump & isGrounded) 
        {
            isGrounded = false;
            _rigidbody.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);

            if (!isLoacl)
                anim.Play("jump");
        }
        jump = false;
    }

    public virtual void OnDeath()
    {
        if (isLoacl)
        {
            gunCamera.gameObject.SetActive(false);
            anim.transform.parent = null;
            anim.enabled = false;
            anim.gameObject.SetActive(true);
        }
        else 
        {
            anim.enabled = false;
        }
        var rigs = anim.GetComponentsInChildren<Rigidbody>();
        var colls = anim.GetComponentsInChildren<Collider>();
        foreach (var rig in rigs)
        {
            rig.isKinematic = false;
        }
        foreach (var coll in colls)
        {
            coll.isTrigger = false;
        }
    }
}