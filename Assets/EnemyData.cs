﻿using Net;

[ProtoBuf.ProtoContract(ImplicitFields = ProtoBuf.ImplicitFields.AllPublic)]
[System.Serializable]
public class EnemyData
{
    public string name;
    public float hp;
    public Vector3 pos;
    public Quaternion rotation;

    public EnemyData() { }

    public EnemyData(string name)
    {
        this.name = name;
    }
}