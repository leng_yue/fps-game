﻿using UnityEngine;

//旋转轴
public enum RotationAxes
{
	MouseXAndY = 0, MouseX = 1, MouseY = 2
}

public class MouseLook : MonoBehaviour
{
	//x轴（水平）速度
	public float sensitivityX = 15F;
	//y轴（垂直）速度
	public float sensitivityY = 15F;
	//x轴（水平）最小旋转值
	public float minimumX = -360F;
	//x轴（水平）最大旋转值
	public float maximumX = 360F;
	//y轴（垂直）最小旋转值
	public float minimumY = -60F;
	//y轴（垂直）最大旋转值
	public float maximumY = 60F;
	//旋转轴
	public RotationAxes axes = RotationAxes.MouseXAndY;
	private float rotationY = 0F;
	internal float MouseY;
	internal float MouseX;

	void Update()
	{
		if (axes == RotationAxes.MouseX)
		{
			transform.Rotate(0, MouseX * sensitivityX, 0);
		}
		else if (axes == RotationAxes.MouseY)
		{
			rotationY += MouseY * sensitivityY;
			rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

			transform.localEulerAngles = new Vector3(-rotationY, transform.localEulerAngles.y, 0);
		}
	}
}