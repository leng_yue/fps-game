﻿using Net.Component;
using Net.Component.Client;
using UnityEngine;
public class Enemy:MonoBehaviour
{
    public EnemyData dat;
    public bool localSync;
    private float time;
    public FPSPlayer target;
    public Animator animator;
    internal bool isDeath;
    public int state;
    private float atttime;

    internal void Init() {
        transform.position = dat.pos;
        transform.rotation = dat.rotation;
        name = dat.name;
    }

    private void Update()
    {
        if (isDeath)
            return;
        if (localSync)
        {
            time += Time.deltaTime;
            if (time >= 1f / 60f)
            {
                time = 0;
                ClientManager.AddOperation(new Net.Share.Operation(Command.EnemySync, name, transform.position, transform.rotation)
                {
                    index = state
                });
            }
            if (target == null)
            {
                localSync = false;
                state = 0;
                return;
            }
            if (target.hp <= 0) 
            {
                target = null;
                localSync = false;
                state = 0;
                ClientManager.AddOperation(new Net.Share.Operation(Command.EnemySync, name, transform.position, transform.rotation)
                {
                    index = state
                });
                return;
            }
            if (Time.time < atttime)
                return;
            var dis = Vector3.Distance(transform.position, target.transform.position);
            if (dis > 3f)
            {
                transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z), Vector3.up);
                transform.Translate(0, 0, 6f * Time.deltaTime);
                state = 1;
            }
            else
            {
                state = 2;
                atttime = Time.time + 3f;
                Invoke(nameof(Attack), 1.5f);
            }
        }
        else 
        {
            transform.position = Vector3.Lerp(transform.position, dat.pos, 0.25f);
            transform.rotation = Quaternion.Lerp(transform.rotation, dat.rotation, 0.25f); 
        }
        switch (state) {
            case 0:
                animator.Play("Idle");
                break;
            case 1:
                animator.Play("Walk");
                break;
            case 2:
                animator.Play("Attack01");
                break;
            case 3:
                animator.Play("Die");
                break;
        }
    }

    void Attack() 
    {
        if (target == null)
            return;
        var dis = Vector3.Distance(transform.position, target.transform.position);
        if (dis > 3f)
            return;
        ClientManager.AddOperation(new Net.Share.Operation(Command.Damage, "", target.playName) { health = 35f });
    }

    internal void OnDeath()
    {
        animator.Play("Die");
        Destroy(GetComponent<Rigidbody>());
        Destroy(GetComponent<Collider>());
    }
}