﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IKLookAt : MonoBehaviour
{
    public Animator anim;
    public Transform IKLookAtTarget;

    private void OnAnimatorIK(int layerIndex)
    {
        anim.SetLookAtWeight(1f, 1f);
        anim.SetLookAtPosition(IKLookAtTarget.position);
    }
}
