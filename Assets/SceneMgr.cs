﻿using Net.Component;
using Net.Component.Client;
using Net.Share;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;

public class SceneMgr : SingleCase<SceneMgr>
{
    public FPSPlayer player;
    public Enemy enemy;
    private Dictionary<string, FPSPlayer> players = new Dictionary<string, FPSPlayer>();
    private Dictionary<string, Enemy> enemies = new Dictionary<string, Enemy>();
    internal Net.Vector3 direction;
    internal Net.Vector3 mouseDir;
    private bool fire;
    private bool jump;
    internal bool run;
    internal FPSPlayer localPlayer;
   

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        ClientManager.Instance.client.OnOperationSync += OnOperationSync;
        ClientManager.Instance.client.AddRpcHandle(this);
        ClientManager.Instance.client.SendRT(NetCmd.CallRpc, "GetEnemys");
        run = true;
        Task.Run(()=> {
            while (run) 
            {
                Thread.Sleep(33);
                var name = ClientManager.Identify;
                if (string.IsNullOrEmpty(name))
                    continue;
                if (players.TryGetValue(name, out FPSPlayer p))
                {
                    var buffer = new byte[8];
                    Buffer.BlockCopy(BitConverter.GetBytes(mouseDir.x), 0, buffer, 0, 4);
                    Buffer.BlockCopy(BitConverter.GetBytes(mouseDir.y), 0, buffer, 4, 4);
                    ClientManager.AddOperation(new Operation(Command.Input, name, direction)
                    {
                        position = p.position,
                        rotation = p.rotation,
                        cmd1 = (byte)(fire ? 1 : 0),
                        cmd2 = (byte)(jump ? 1 : 0),
                        buffer = buffer
                    });
                    fire = false;
                    jump = false;
                }
            }
        });
    }

    void CreatePlayer(Operation opt) 
    {
        var p = Instantiate(player);
        p.name = opt.name;
        p.playName = opt.name;
        p.transform.position = opt.position;
        p.transform.rotation = opt.rotation;
        p.position = opt.position;
        p.rotation = opt.rotation;
        p.isLoacl = opt.name == ClientManager.Identify;
        players.Add(opt.name, p);
        if (ClientManager.IsLocal(opt.name))
        {
            p.gunCamera.enabled = true;
            p.mainCamera.enabled = true;
            p.anim.gameObject.SetActive(false);
            localPlayer = p;
        }
        else
        {
            p.handle.SetActive(false);
        }
    }

    private void OnOperationSync(OperationList obj)
    {
        foreach (var opt in obj.operations) {
            switch (opt.cmd) {
                case Command.CreatePlayer:
                    if (!players.ContainsKey(opt.name))
                        CreatePlayer(opt);
                    break;
                case Command.Input:
                    if (!players.ContainsKey(opt.name)) 
                        CreatePlayer(opt);
                    var p1 = players[opt.name];
                    p1.direction = opt.direction;
                    if (!p1.isLoacl)
                    {
                        p1.position = opt.position;
                        p1.rotation = opt.rotation;
                    }
                    else 
                    {
                        p1.mouseX.MouseX = BitConverter.ToSingle(opt.buffer, 0);
                    }
                    p1.mouseY.MouseY = BitConverter.ToSingle(opt.buffer, 4);
                    if (opt.cmd1 == 1)
                        p1.fire = true;
                    if (opt.cmd2 == 1)
                        p1.jump = true;
                    if (p1.isDeath)
                        continue;
                    p1.hp = opt.health;
                    if (opt.health <= 0)
                    {
                        p1.isDeath = true;
                        p1.OnDeath();
                    }
                    break;
                case Command.EnemyDamage:
                    if (enemies.ContainsKey(opt.name1))
                    {
                        var p2 = enemies[opt.name1];
                        p2.dat.hp = opt.health;
                        if (opt.health <= 0 & !p2.isDeath) {
                            p2.isDeath = true;
                            p2.OnDeath();
                        }
                    }
                    break;
                case Command.EnemySync:
                    if (enemies.ContainsKey(opt.name))
                    {
                        var p2 = enemies[opt.name];
                        p2.state = opt.index;
                        p2.dat.pos = opt.position;
                        p2.dat.rotation = opt.rotation;
                    }
                    break;
                case NetCmd.QuitGame:
                    if (players.ContainsKey(opt.name)) {
                        var p2 = players[opt.name];
                        Destroy(p2.gameObject);
                        players.Remove(opt.name);
                    }
                    break;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        direction = new Net.Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        mouseDir = new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"), 0);
        if (Input.GetMouseButtonDown(0)) 
            fire = true;
        if (Input.GetKeyDown(KeyCode.Space))
            jump = true;

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            if (localPlayer != null) {
                localPlayer.mouseX.enabled = false;
                localPlayer.mouseY.enabled = false;
            }
        }
        if (Input.GetKeyDown(KeyCode.Mouse0)) 
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
                if (localPlayer != null)
                {
                    localPlayer.mouseX.enabled = true;
                    localPlayer.mouseY.enabled = true;
                }
            }
        }
    }

    [Rpc]
    void InsEmys(List<EnemyData> enemies) 
    {
        foreach (var dat in enemies) {
            var emy = Instantiate(enemy);
            emy.dat = dat;
            emy.Init();
            this.enemies.Add(dat.name, emy);
            if (dat.hp <= 0)
            {
                emy.isDeath = true;
                emy.OnDeath();
            }
        }
    }

    private void OnDestroy()
    {
        ClientManager.Instance.client.OnOperationSync -= OnOperationSync;
        run = false;
    }
}
