﻿using System;

namespace Net.Share
{
    /// <summary>
    /// 远程过程调用模型,此类负责网络通讯中数据解析临时存储的对象
    /// </summary>
    public struct RPCModel
    {
        /// <summary>
        /// 内核? true:数据经过框架内部序列化 false:数据由开发者自己处理
        /// </summary>
        public bool kernel;
        /// <summary>
        /// 网络指令
        /// </summary>
        public byte cmd;
        public byte[] buffer;
        public int index, size;
        /// <summary>
        /// 数据缓冲器
        /// </summary>
        public byte[] Buffer
        {
            get
            {
                if (size == 0)
                    return null;
                byte[] buffer1 = new byte[size];
                System.Buffer.BlockCopy(buffer, index, buffer1, 0, size);
                return buffer1;
            }
            set
            {
                buffer = value;
                size = value.Length;
            }
        }
        /// <summary>
        /// 远程函数名
        /// </summary>
        public string func;
        /// <summary>
        /// 远程方法遮罩值
        /// </summary>
        public ushort methodMask;
        /// <summary>
        /// 远程参数
        /// </summary>
        public object[] pars;
        /// <summary>
        /// 数据是否经过内部序列化?
        /// </summary>
        public bool serialize;

        /// <summary>
        /// 构造
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="buffer"></param>
        public RPCModel(byte cmd, byte[] buffer)
        {
            kernel = false;
            this.cmd = cmd;
            this.buffer = buffer;
            func = null;
            pars = null;
            serialize = false;
            index = 0;
            size = buffer.Length;
            methodMask = 0;
        }

        /// <summary>
        /// 构造Send
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="func"></param>
        /// <param name="pars"></param>
        public RPCModel(byte cmd, string func, object[] pars)
        {
            kernel = true;
            serialize = true;
            this.cmd = cmd;
            this.func = func;
            this.pars = pars;
            buffer = null;
            index = 0;
            size = 0; 
            methodMask = 0;
        }

        public RPCModel(byte cmd, ushort methodMask, object[] pars)
        {
            kernel = true;
            serialize = true;
            this.cmd = cmd;
            func = string.Empty;
            this.methodMask = methodMask;
            this.pars = pars;
            buffer = null;
            index = 0;
            size = 0;
        }

        /// <summary>
        /// 构造Send
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="buffer"></param>
        /// <param name="kernel"></param>
        public RPCModel(byte cmd, byte[] buffer, bool kernel)
        {
            this.cmd = cmd;
            this.buffer = buffer;
            this.kernel = kernel;
            func = null;
            pars = null;
            serialize = false;
            index = 0;
            size = buffer.Length;
            methodMask = 0;
        }

        public RPCModel(byte cmd, bool kernel, byte[] buffer, int index, int size)
        {
            this.cmd = cmd;
            this.buffer = buffer;
            this.index = index;
            this.size = size;
            this.kernel = kernel;
            func = null;
            pars = null;
            serialize = false;
            methodMask = 0;
        }

        /// <summary>
        /// 构造SendRT可靠传输
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="buffer"></param>
        /// <param name="kernel"></param>
        /// <param name="serialize"></param>
        public RPCModel(byte cmd, byte[] buffer, bool kernel, bool serialize)
        {
            this.cmd = cmd;
            this.buffer = buffer;
            this.kernel = kernel;
            this.serialize = serialize;
            func = null;
            pars = null;
            index = 0;
            size = buffer.Length;
            methodMask = 0; 
        }

        public RPCModel(byte cmd, byte[] buffer, bool kernel, bool serialize, ushort methodMask)
        {
            this.cmd = cmd;
            this.buffer = buffer;
            this.kernel = kernel;
            this.serialize = serialize;
            func = null;
            pars = null;
            index = 0;
            size = buffer.Length;
            this.methodMask = methodMask;
        }

        /// <summary>
        /// 构造SendRT可靠传输
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="func"></param>
        /// <param name="pars"></param>
        /// <param name="kernel"></param>
        /// <param name="serialize"></param>
        public RPCModel(byte cmd, string func, object[] pars, bool kernel, bool serialize)
        {
            this.cmd = cmd;
            this.func = func;
            this.pars = pars;
            this.kernel = kernel;
            this.serialize = serialize;
            buffer = null;
            index = 0;
            size = 0;
            methodMask = 0;
        }

        public RPCModel(byte cmd, string func, object[] pars, bool kernel, bool serialize, ushort methodMask)
        {
            this.cmd = cmd;
            this.func = func;
            this.pars = pars;
            this.kernel = kernel;
            this.serialize = serialize;
            buffer = null;
            index = 0;
            size = 0;
            this.methodMask = methodMask;
        }

        /// <summary>
        /// 讲类转换字符串
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            System.Reflection.FieldInfo[] fields = typeof(NetCmd).GetFields(System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
            string cmdStr = "";
            if (cmd < fields.Length)
                cmdStr = fields[cmd].Name;
            return $"指令:{cmdStr} 内核:{kernel} 方法:{func} 数据:{(buffer != null ? buffer.Length : 0)}";
        }
    }
}
