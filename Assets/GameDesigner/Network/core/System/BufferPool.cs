﻿using Net.Event;
using System;

namespace Net.Share
{
    /// <summary>
    /// 内存数据片段
    /// </summary>
    public class Segment : IDisposable
    {
        /// <summary>
        /// 总内存
        /// </summary>
        public byte[] Buffer { get; internal set; }
        /// <summary>
        /// 片的开始位置
        /// </summary>
        public int Index { get; set; }
        /// <summary>
        /// 片的长度
        /// </summary>
        public int Count { get; set; }
        /// <summary>
        /// 读写位置
        /// </summary>
        public int Position { get; set; }

        internal bool isDespose;
        internal int length;
        private bool isRecovery;
        /// <summary>
        /// 获取总长度
        /// </summary>
        public int Length { get { return length; } }

        /// <summary>
        /// 获取或设置总内存位置索引
        /// </summary>
        /// <param name="index">内存位置索引</param>
        /// <returns></returns>
        public byte this[int index] { get { return Buffer[index]; } set { Buffer[index] = value; } }

        /// <summary>
        /// 构造内存分片
        /// </summary>
        /// <param name="buffer"></param>
        public Segment(byte[] buffer) : this(buffer, 0, buffer.Length)
        {
        }

        /// <summary>
        /// 构造内存分片
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="index"></param>
        /// <param name="count"></param>
        public Segment(byte[] buffer, int index, int count, bool isRecovery = true)
        {
            Buffer = buffer;
            Index = index;
            Count = count;
            length = count;
            isDespose = false;
            this.isRecovery = isRecovery;
        }

        public static implicit operator Segment(byte[] buffer)
        {
            return new Segment(buffer);
        }

        public static implicit operator byte[](Segment segment)
        {
            return segment.Buffer;
        }

        ~Segment()
        {
            if (isRecovery) 
                NDebug.LogError("分片内存被泄漏了!");
            Dispose();
        }

        public void Dispose()
        {
            if (!isRecovery)
                return;
            BufferPool.Push(this);
        }

        public override string ToString()
        {
            return $"byte[{(Buffer != null ? Buffer.Length : 0)}] index:{Index} count:{Count} size:{Count-Index}";
        }

        /// <summary>
        /// 复制分片数据
        /// </summary>
        /// <param name="recovery">复制数据后立即回收此分片?</param>
        /// <returns></returns>
        public byte[] ToArray(bool recovery = false)
        {
            if (Position > Count) Count = Position;
            byte[] array = new byte[Count];
            System.Buffer.BlockCopy(Buffer, Index, array, 0, Count);
            if (recovery) BufferPool.Push(this);
            return array;
        }

        public void WriteByte(byte value)
        {
            Buffer[Position++] = value;
            Count = Position;
        }

        public unsafe void Write(byte[] buffer, int index, int count)
        {
            System.Buffer.BlockCopy(buffer, index, Buffer, Position, count);
            Position += count;
            Count = Position;
        }
    }

    /// <summary>
    /// 数据缓冲内存池
    /// </summary>
    public static class BufferPool
    {
        /// <summary>
        /// 数据缓冲池大小. 默认65536字节
        /// </summary>
        public static int Size { get; set; } = 65536;
        private static readonly StackSafe<Segment>[] STACKS = new StackSafe<Segment>[23];
        private static readonly int[] TABLE = new int[] {
            256,512,1024,2048,4096,8192,16384,32768,65536,131072,262144,524288,1048576,2097152,4194304,8388608,16777216,33554432,67108864,134217728,268435456,536870912,1073741824
        };

        static BufferPool() 
        {
            for (int i = 0; i < TABLE.Length; i++)
            {
                STACKS[i] = new StackSafe<Segment>();
            }
        }

        /// <summary>
        /// 从内存池取数据片
        /// </summary>
        /// <returns></returns>
        public static Segment Take()
        {
            return Take(Size);
        }

        /// <summary>
        /// 从内存池取数据片
        /// </summary>
        /// <param name="size">内存大小</param>
        /// <returns></returns>
        public static Segment Take(int size)
        {
            var tableInx = 0;
            for (int i = 0; i < TABLE.Length; i++)
            {
                if (size <= TABLE[i])
                {
                    size = TABLE[i];
                    tableInx = i;
                    goto J;
                }
            }
            J: var stack = STACKS[tableInx];
            if (stack.TryPop(out Segment segment)) 
            {
                segment.isDespose = false;
                segment.Index = 0;
                segment.Count = 0;
                segment.Position = 0;
                return segment;
            }
            return new Segment(new byte[size]);
        }

        /// <summary>
        /// 压入数据片, 等待复用
        /// </summary>
        /// <param name="segment"></param>
        public static void Push(Segment segment) 
        {
            if (segment.isDespose)
                return;
            segment.isDespose = true;
            for (int i = 0; i < TABLE.Length; i++)
            {
                if (segment.length == TABLE[i])
                {
                    STACKS[i].Push(segment);
                    return;
                }
            }
        }
    }

    public static class ObjectPool<T> where T : new()
    {
        private static readonly StackSafe<T> STACK = new StackSafe<T>();

        public static void Init(int poolSize)
        {
            for (int i = 0; i < poolSize; i++)
            {
                STACK.Push(new T());
            }
        }

        public static T Take()
        {
            if (STACK.TryPop(out T obj))
                return obj;
            return new T();
        }

        public static void Push(T obj)
        {
            STACK.Push(obj);
        }
    }
}
