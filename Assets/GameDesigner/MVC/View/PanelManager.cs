﻿#if UNITY_STANDALONE || UNITY_ANDROID || UNITY_IOS
namespace MVC.View
{
    using Net.Component.Client;
    using UnityEngine;

    public class PanelManager : SingleCase<PanelManager>
    {
        public Transform[] panels;
    }
}
#endif