﻿#if UNITY_STANDALONE || UNITY_ANDROID || UNITY_IOS
namespace MVC.View
{
    using Net.Component.Client;
    using System.Collections.Generic;
    using UnityEngine;

    public class FieldManager : SingleCase<FieldManager>
    {
        private readonly Dictionary<string, FieldCollection> fieldDic = new Dictionary<string, FieldCollection>();

        private void Awake()
        {
            Initialize();
        }

        public void Initialize() 
        {
            var fields = Resources.FindObjectsOfTypeAll<FieldCollection>();
            foreach (var field in fields)
            {
                if (string.IsNullOrEmpty(field.fieldName))
                    field.fieldName = field.name;
                fieldDic.Add(field.fieldName, field);
            }
        }

        public FieldCollection this[string name]
        {
            get
            {
                fieldDic.TryGetValue(name, out FieldCollection field);
                return field;
            }
        }
    }
}
#endif