﻿#if SERVICE
namespace Net.Component.MMORPG_Server
{
    /// <summary>
    /// 数据库组件
    /// </summary>
    public class DBComponent : Server.DataBase<PlayerData>
    {
        /// <summary>
        /// 数据库单例
        /// </summary>
        public static DBComponent Instance = new DBComponent();
    }
}
#endif