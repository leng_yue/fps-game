﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class test : MonoBehaviour
{
    public Transform target;

    public bool e;

    // Update is called once per frame
    void Update()
    {
        if (!e)
            return;
        e = false;

        //var rigs222 = target.GetComponentsInChildren<CharacterJoint>();

        //for (int i = 0; i < rigs222.Length; i++)
        //{
        //    DestroyImmediate(rigs222[i], true);
        //}

        //var rigs444 = target.GetComponentsInChildren<CapsuleCollider>();

        //for (int i = 0; i < rigs444.Length; i++)
        //{
        //    DestroyImmediate(rigs444[i], true);
        //}

        //var rigs111 = target.GetComponentsInChildren<Rigidbody>();

        //for (int i = 0; i < rigs111.Length; i++)
        //{
        //    DestroyImmediate(rigs111[i], true);
        //}
        //return;

        var rigs = transform.GetComponentsInChildren<Rigidbody>();
        var trans = target.GetComponentsInChildren<Transform>();
        foreach (var rig in rigs) 
        {
            for (int i = 0; i < trans.Length;i++)
            {
                if (trans[i].name == rig.name) 
                {
                    if (trans[i].gameObject.GetComponent<Rigidbody>() != null)
                        continue;
                    var rig2 = trans[i].gameObject.AddComponent<Rigidbody>();
                    rig2.mass = rig.mass;
                    rig2.drag = rig.drag;
                    rig2.angularDrag = rig.angularDrag;
                    rig2.useGravity = rig.useGravity;
                    rig2.interpolation = rig.interpolation;
                    rig2.collisionDetectionMode = rig.collisionDetectionMode;

                    var cc = rig.GetComponent<CapsuleCollider>();
                    if (cc != null) 
                    {
                        var cc1 = rig2.gameObject.AddComponent<CapsuleCollider>();
                        cc1.isTrigger = cc.isTrigger;
                        cc1.center = cc.center;
                        cc1.radius = cc.radius;
                        cc1.height = cc.height;
                        cc1.direction = cc.direction;
                    }

                    var box = rig.GetComponent<BoxCollider>();
                    if (box != null)
                    {
                        var box1 = rig2.gameObject.AddComponent<BoxCollider>();
                        box1.isTrigger = box.isTrigger;
                        box1.center = box.center;
                        box1.size = box.size;
                    }

                    var cj = rig.GetComponent<CharacterJoint>();
                    if (cj != null)
                    {
                        var cj1 = rig2.gameObject.AddComponent<CharacterJoint>();
                        //cj1.connectedBody = rig2.GetComponentInParent<Rigidbody>();
                        cj1.anchor = cj.anchor;
                        cj1.axis = cj.axis;
                        cj1.autoConfigureConnectedAnchor = cj.autoConfigureConnectedAnchor;
                        cj1.connectedAnchor = cj.connectedAnchor;
                        cj1.swingAxis = cj.swingAxis;
                        cj1.twistLimitSpring = cj.twistLimitSpring;
                        cj1.lowTwistLimit = cj.lowTwistLimit;
                        cj1.highTwistLimit = cj.highTwistLimit;
                        cj1.swingLimitSpring = cj.swingLimitSpring;
                        cj1.swing1Limit = cj.swing1Limit;
                        cj1.swing2Limit = cj.swing2Limit;
                        cj1.enableProjection = cj.enableProjection;
                        cj1.projectionDistance = cj.projectionDistance;
                        cj1.projectionAngle = cj.projectionAngle;
                        cj1.breakForce = cj.breakForce;
                        cj1.breakTorque = cj.breakTorque;
                        cj1.enablePreprocessing = cj.enablePreprocessing;
                        cj1.massScale = cj.massScale;
                        cj1.connectedMassScale = cj.connectedMassScale;
                    }
                    break;
                }
            }
        }
        
    }
}
