﻿using Net.Component.Client;
using Net.Share;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIMgr : MonoBehaviour
{
    public Button button;

    private void Awake()
    {
        ClientManager.Instance.client.AddRpcHandle(this);
    }

    // Start is called before the first frame update
    void Start()
    {
        button.onClick.AddListener(()=> {
            ClientManager.Instance.SendRT(NetCmd.EntityRpc, "Revive");
        });
    }

    [Rpc]
    void Revive() 
    {
        SceneManager.LoadScene(1);
    }
}
